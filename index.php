<?php
/**
 * Created with PhpStorm
 * Created by Johnson Lawal
 * User: AppZone
 * Date: 1/20/2018
 * Time: 10:55 AM
 */

require 'vendor/autoload.php';
require __DIR__ . '/protected/propel_assets/generated-conf/config.php';

use MoonlightAdminEntity\MoonlightAdminEntity\User as User;
use MoonlightAdminEntity\MoonlightAdminEntity\UserQuery as UserQuery;
use MoonlightAdminEntity\MoonlightAdminEntity\Advert as Advert;
use MoonlightAdminEntity\MoonlightAdminEntity\AdvertQuery as AdvertQuery;
use MoonlightAdminEntity\MoonlightAdminEntity\Exchange as Exchange;
use MoonlightAdminEntity\MoonlightAdminEntity\ExchangeQuery as ExchangeQuery;
use MoonlightAdminEntity\MoonlightAdminEntity\Transaction as Transaction;
use MoonlightAdminEntity\MoonlightAdminEntity\TransactionQuery as TransactionQuery;

$config['displayErrorDetails'] = true;
$config['addContentLengthHeader'] = false;

$app = new Slim\App(["settings" => $config]);

$app->get('/hello/{name}', function ($request, $response, $args) {
    return $response->getBody()->write("Hello, " . $args['name']);

});

$app->group('/user', function () {

    $this->post('/create', function ($request, $response, $args) {

        $parsedBody = $request->getParsedBody();
        $user = new User();

        $user->setUsername($parsedBody['username']);
        $user->setPassword(sha1($parsedBody['password']));
        $user->setFirstname($parsedBody['firstname']);
        $user->setLastname($parsedBody['lastname']);
        $user->setAddress($parsedBody['address']);
        $user->setVerified($parsedBody['verified']);
        $user->setDatecreated(new DateTime());

        $user->save();
        return $response->getBody()->write($user->toJson());

    });

    $this->post('/read', function ($request, $response, $args) {

        $username = $request->getParsedBody("username", $default = null);
        $user = UserQuery::create()
            ->filterByUsername($username)
            ->findOne();

        return $response->getBody()->write($user->toJson());

    });

    $this->post('/update', function ($request, $response, $args) {

        $username = $request->getParsedBody("username", $default = null);
        $parsedBody = $request->getParsedBody("username", $default = null);
        $user = UserQuery::create()
            ->filterByUsername($username)
            ->findOne();

        !empty($parsedBody['password']) ? $user->setPassword(sha1($parsedBody['password'])) : "";
        !empty($parsedBody['firstname']) ? $user->setFirstname($parsedBody['firstname']) : "";
        !empty($parsedBody['lastname']) ? $user->setLastname($parsedBody['lastname']): "";
        !empty($parsedBody['address']) ? $user->setAddress($parsedBody['address']) : "";
        !empty($parsedBody['verified']) ? $user->setVerified($parsedBody['verified']) : "";

        $user->save();
        return $response->getBody()->write($user->toJson());

    });
});

$app->group('/advert', function () {

    $this->post('/create', function ($request, $response, $args) {

        $parsedBody = $request->getParsedBody();
        $exchangeId = $request->getParsedBody("exchange_id", $default = null);
        $username = $request->getParsedBody("username", $default = null);

        $exchange = ExchangeQuery::create()
            ->findById($exchangeId);

        $user = UserQuery::create()
            ->filterByUsername($username)
            ->findOne();

        $advert = new Advert();

        //0 is for sell, while 1 is for buy
        $advert->setType($parsedBody['type']=="sell"? 0 : 1);
        $advert->setRate($exchange);
        $advert->setApi("");
        $advert->setDatecreated(new DateTime());
        $advert->setUser($user);

        $advert->save();
        return $response->getBody()->write($advert->toJson());

    });

    $this->post('/read', function ($request, $response, $args) {

        $username = $request->getParsedBody("username", $default = null);
        $user = UserQuery::create()
            ->filterByUsername($username)
            ->findOne();
        $advert = AdvertQuery::create()
            ->filterByUser($user)
            ->find();

        return $response->getBody()->write($advert->toJson());

    });

    $this->post('/update', function ($request, $response, $args) {

        $parsedBody = $request->getParsedBody();
        $advertId = $request->getParsedBody("advert_id", $default = null);
        $advert = AdvertQuery::create()
            ->findById($advertId);

        //!empty($parsedBody['exchange_id']) ? $advert->setRate($exchange): "";

        $advert->save();
        return $response->getBody()->write($advert->toJson());

    });
});

$app->group('/exchange', function () {

    $this->post('/create', function ($request, $response, $args) {

        $parsedBody = $request->getParsedBody();
        $exchange = new Exchange();

        $exchange->setName($parsedBody['name']);
        $exchange->setDatecreated(new DateTime());

        $exchange->save();
        return $response->getBody()->write($exchange->toJson());

    });

    $this->post('/read', function ($request, $response, $args) {

        $id = $request->getParsedBody("id", $default = null);
        $exchange = ExchangeQuery::create()
            ->filterByUsername($id)
            ->findOne();

        return $response->getBody()->write($exchange->toJson());

    });

    $this->post('/update', function ($request, $response, $args) {

        $exchangeId = $request->getParsedBody("exchange_id", $default = null);
        $exchange = ExhangeQuery::create()
            ->findById($exchangeId);

        !empty($parsedBody['name']) ? $exchange->setName($parsedBody['name']) : "";

        $exchange->save();
        return $response->getBody()->write($exchange->toJson());

    });
});


$app->group('/transaction', function () {

    $this->post('/create', function ($request, $response, $args) {

        $parsedBody = $request->getParsedBody();
        $debitUserUsername = $request->getParsedBody('debit_username');
        $creditUserUsername = $request->getParsedBody('credit_username');

        $debitUser = UserQuery::create()->filterByUsername($debitUserUsername)->findOne();
        $creditUser = UserQuery::create()->filterByUsername($creditUserUsername)->findOne();

        $transaction = new Transaction();

        $transaction->setUserRelatedByDebitUser($debitUser);
        $transaction->setUserRelatedByCreditUser($creditUser);
        $transaction->setAmount($parsedBody['amount']);
        $transaction->setDescription(['description']);
        $transaction->setDatecreated(new DateTime());
        $transaction->setAdvert(new DateTime());

        $transaction->save();
        return $response->getBody()->write($transaction->toJson());

    });

    $this->post('/read', function ($request, $response, $args) {

        $id = $request->getParsedBody("id", $default = null);
        $transaction = TransactionQuery::create()
            ->filterByUsername($id)
            ->findOne();

        return $response->getBody()->write($transaction->toJson());

    });

    $this->post('/update', function ($request, $response, $args) {

        $parsedBody = $request->getParsedBody("username", $default = null);
        $transaction = TransactionQuery::create()
            ->filterByFirstname($parsedBody)
            ->findOne();

        //transactions should not be updatable

        return $response->getBody()->write($transaction->toJson());

    });
});


//User:create, read, update
//Adverts: create, read, update
//AppConfig: create, read, update
//Exchanges: create, read, update
//Transactions: create, read, update

$app->group('/users/{id:[0-9]+}', function () {
    $this->get('/hello/{name}', function ($request, $response, $args) {
        return $response->getBody()->write("Yello, " . $args['name']);
    });
    $this->map(['GET', 'DELETE', 'PATCH', 'PUT'], '', function ($request, $response, $args) {
        // Find, delete, patch or replace user identified by $args['id']
    })->setName('user');
    $this->get('/reset-password', function ($request, $response, $args) {
        // Route for /users/{id:[0-9]+}/reset-password
        // Reset the password for user identified by $args['id']
    })->setName('user-password-reset');
});

$app->run();