<?php

namespace MoonlightAdminEntity\MoonlightAdminEntity;

use MoonlightAdminEntity\MoonlightAdminEntity\Base\Advert as BaseAdvert;

/**
 * Skeleton subclass for representing a row from the 'advert' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class Advert extends BaseAdvert
{

}
