<?php

namespace MoonlightAdminEntity\MoonlightAdminEntity;

use MoonlightAdminEntity\MoonlightAdminEntity\Base\ExchangeQuery as BaseExchangeQuery;

/**
 * Skeleton subclass for performing query and update operations on the 'exchange' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class ExchangeQuery extends BaseExchangeQuery
{

}
