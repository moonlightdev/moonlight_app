<?php

namespace MoonlightAdminEntity\MoonlightAdminEntity\Base;

use \Exception;
use \PDO;
use MoonlightAdminEntity\MoonlightAdminEntity\Transaction as ChildTransaction;
use MoonlightAdminEntity\MoonlightAdminEntity\TransactionQuery as ChildTransactionQuery;
use MoonlightAdminEntity\MoonlightAdminEntity\Map\TransactionTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'transaction' table.
 *
 *
 *
 * @method     ChildTransactionQuery orderById($order = Criteria::ASC) Order by the id column
 * @method     ChildTransactionQuery orderByDebitUser($order = Criteria::ASC) Order by the debit_user column
 * @method     ChildTransactionQuery orderByCreditUser($order = Criteria::ASC) Order by the credit_user column
 * @method     ChildTransactionQuery orderByAmount($order = Criteria::ASC) Order by the amount column
 * @method     ChildTransactionQuery orderByDescription($order = Criteria::ASC) Order by the description column
 * @method     ChildTransactionQuery orderByDatecreated($order = Criteria::ASC) Order by the datecreated column
 * @method     ChildTransactionQuery orderByLastmodified($order = Criteria::ASC) Order by the lastmodified column
 * @method     ChildTransactionQuery orderByAdvertId($order = Criteria::ASC) Order by the advert_id column
 *
 * @method     ChildTransactionQuery groupById() Group by the id column
 * @method     ChildTransactionQuery groupByDebitUser() Group by the debit_user column
 * @method     ChildTransactionQuery groupByCreditUser() Group by the credit_user column
 * @method     ChildTransactionQuery groupByAmount() Group by the amount column
 * @method     ChildTransactionQuery groupByDescription() Group by the description column
 * @method     ChildTransactionQuery groupByDatecreated() Group by the datecreated column
 * @method     ChildTransactionQuery groupByLastmodified() Group by the lastmodified column
 * @method     ChildTransactionQuery groupByAdvertId() Group by the advert_id column
 *
 * @method     ChildTransactionQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildTransactionQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildTransactionQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildTransactionQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildTransactionQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildTransactionQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildTransactionQuery leftJoinUserRelatedByDebitUser($relationAlias = null) Adds a LEFT JOIN clause to the query using the UserRelatedByDebitUser relation
 * @method     ChildTransactionQuery rightJoinUserRelatedByDebitUser($relationAlias = null) Adds a RIGHT JOIN clause to the query using the UserRelatedByDebitUser relation
 * @method     ChildTransactionQuery innerJoinUserRelatedByDebitUser($relationAlias = null) Adds a INNER JOIN clause to the query using the UserRelatedByDebitUser relation
 *
 * @method     ChildTransactionQuery joinWithUserRelatedByDebitUser($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the UserRelatedByDebitUser relation
 *
 * @method     ChildTransactionQuery leftJoinWithUserRelatedByDebitUser() Adds a LEFT JOIN clause and with to the query using the UserRelatedByDebitUser relation
 * @method     ChildTransactionQuery rightJoinWithUserRelatedByDebitUser() Adds a RIGHT JOIN clause and with to the query using the UserRelatedByDebitUser relation
 * @method     ChildTransactionQuery innerJoinWithUserRelatedByDebitUser() Adds a INNER JOIN clause and with to the query using the UserRelatedByDebitUser relation
 *
 * @method     ChildTransactionQuery leftJoinUserRelatedByCreditUser($relationAlias = null) Adds a LEFT JOIN clause to the query using the UserRelatedByCreditUser relation
 * @method     ChildTransactionQuery rightJoinUserRelatedByCreditUser($relationAlias = null) Adds a RIGHT JOIN clause to the query using the UserRelatedByCreditUser relation
 * @method     ChildTransactionQuery innerJoinUserRelatedByCreditUser($relationAlias = null) Adds a INNER JOIN clause to the query using the UserRelatedByCreditUser relation
 *
 * @method     ChildTransactionQuery joinWithUserRelatedByCreditUser($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the UserRelatedByCreditUser relation
 *
 * @method     ChildTransactionQuery leftJoinWithUserRelatedByCreditUser() Adds a LEFT JOIN clause and with to the query using the UserRelatedByCreditUser relation
 * @method     ChildTransactionQuery rightJoinWithUserRelatedByCreditUser() Adds a RIGHT JOIN clause and with to the query using the UserRelatedByCreditUser relation
 * @method     ChildTransactionQuery innerJoinWithUserRelatedByCreditUser() Adds a INNER JOIN clause and with to the query using the UserRelatedByCreditUser relation
 *
 * @method     ChildTransactionQuery leftJoinAdvert($relationAlias = null) Adds a LEFT JOIN clause to the query using the Advert relation
 * @method     ChildTransactionQuery rightJoinAdvert($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Advert relation
 * @method     ChildTransactionQuery innerJoinAdvert($relationAlias = null) Adds a INNER JOIN clause to the query using the Advert relation
 *
 * @method     ChildTransactionQuery joinWithAdvert($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Advert relation
 *
 * @method     ChildTransactionQuery leftJoinWithAdvert() Adds a LEFT JOIN clause and with to the query using the Advert relation
 * @method     ChildTransactionQuery rightJoinWithAdvert() Adds a RIGHT JOIN clause and with to the query using the Advert relation
 * @method     ChildTransactionQuery innerJoinWithAdvert() Adds a INNER JOIN clause and with to the query using the Advert relation
 *
 * @method     \MoonlightAdminEntity\MoonlightAdminEntity\UserQuery|\MoonlightAdminEntity\MoonlightAdminEntity\AdvertQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildTransaction findOne(ConnectionInterface $con = null) Return the first ChildTransaction matching the query
 * @method     ChildTransaction findOneOrCreate(ConnectionInterface $con = null) Return the first ChildTransaction matching the query, or a new ChildTransaction object populated from the query conditions when no match is found
 *
 * @method     ChildTransaction findOneById(int $id) Return the first ChildTransaction filtered by the id column
 * @method     ChildTransaction findOneByDebitUser(int $debit_user) Return the first ChildTransaction filtered by the debit_user column
 * @method     ChildTransaction findOneByCreditUser(int $credit_user) Return the first ChildTransaction filtered by the credit_user column
 * @method     ChildTransaction findOneByAmount(string $amount) Return the first ChildTransaction filtered by the amount column
 * @method     ChildTransaction findOneByDescription(string $description) Return the first ChildTransaction filtered by the description column
 * @method     ChildTransaction findOneByDatecreated(string $datecreated) Return the first ChildTransaction filtered by the datecreated column
 * @method     ChildTransaction findOneByLastmodified(string $lastmodified) Return the first ChildTransaction filtered by the lastmodified column
 * @method     ChildTransaction findOneByAdvertId(int $advert_id) Return the first ChildTransaction filtered by the advert_id column *

 * @method     ChildTransaction requirePk($key, ConnectionInterface $con = null) Return the ChildTransaction by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildTransaction requireOne(ConnectionInterface $con = null) Return the first ChildTransaction matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildTransaction requireOneById(int $id) Return the first ChildTransaction filtered by the id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildTransaction requireOneByDebitUser(int $debit_user) Return the first ChildTransaction filtered by the debit_user column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildTransaction requireOneByCreditUser(int $credit_user) Return the first ChildTransaction filtered by the credit_user column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildTransaction requireOneByAmount(string $amount) Return the first ChildTransaction filtered by the amount column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildTransaction requireOneByDescription(string $description) Return the first ChildTransaction filtered by the description column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildTransaction requireOneByDatecreated(string $datecreated) Return the first ChildTransaction filtered by the datecreated column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildTransaction requireOneByLastmodified(string $lastmodified) Return the first ChildTransaction filtered by the lastmodified column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildTransaction requireOneByAdvertId(int $advert_id) Return the first ChildTransaction filtered by the advert_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildTransaction[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildTransaction objects based on current ModelCriteria
 * @method     ChildTransaction[]|ObjectCollection findById(int $id) Return ChildTransaction objects filtered by the id column
 * @method     ChildTransaction[]|ObjectCollection findByDebitUser(int $debit_user) Return ChildTransaction objects filtered by the debit_user column
 * @method     ChildTransaction[]|ObjectCollection findByCreditUser(int $credit_user) Return ChildTransaction objects filtered by the credit_user column
 * @method     ChildTransaction[]|ObjectCollection findByAmount(string $amount) Return ChildTransaction objects filtered by the amount column
 * @method     ChildTransaction[]|ObjectCollection findByDescription(string $description) Return ChildTransaction objects filtered by the description column
 * @method     ChildTransaction[]|ObjectCollection findByDatecreated(string $datecreated) Return ChildTransaction objects filtered by the datecreated column
 * @method     ChildTransaction[]|ObjectCollection findByLastmodified(string $lastmodified) Return ChildTransaction objects filtered by the lastmodified column
 * @method     ChildTransaction[]|ObjectCollection findByAdvertId(int $advert_id) Return ChildTransaction objects filtered by the advert_id column
 * @method     ChildTransaction[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class TransactionQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \MoonlightAdminEntity\MoonlightAdminEntity\Base\TransactionQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'default', $modelName = '\\MoonlightAdminEntity\\MoonlightAdminEntity\\Transaction', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildTransactionQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildTransactionQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildTransactionQuery) {
            return $criteria;
        }
        $query = new ChildTransactionQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildTransaction|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(TransactionTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = TransactionTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildTransaction A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT id, debit_user, credit_user, amount, description, datecreated, lastmodified, advert_id FROM transaction WHERE id = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildTransaction $obj */
            $obj = new ChildTransaction();
            $obj->hydrate($row);
            TransactionTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildTransaction|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildTransactionQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(TransactionTableMap::COL_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildTransactionQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(TransactionTableMap::COL_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id > 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildTransactionQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(TransactionTableMap::COL_ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(TransactionTableMap::COL_ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(TransactionTableMap::COL_ID, $id, $comparison);
    }

    /**
     * Filter the query on the debit_user column
     *
     * Example usage:
     * <code>
     * $query->filterByDebitUser(1234); // WHERE debit_user = 1234
     * $query->filterByDebitUser(array(12, 34)); // WHERE debit_user IN (12, 34)
     * $query->filterByDebitUser(array('min' => 12)); // WHERE debit_user > 12
     * </code>
     *
     * @see       filterByUserRelatedByDebitUser()
     *
     * @param     mixed $debitUser The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildTransactionQuery The current query, for fluid interface
     */
    public function filterByDebitUser($debitUser = null, $comparison = null)
    {
        if (is_array($debitUser)) {
            $useMinMax = false;
            if (isset($debitUser['min'])) {
                $this->addUsingAlias(TransactionTableMap::COL_DEBIT_USER, $debitUser['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($debitUser['max'])) {
                $this->addUsingAlias(TransactionTableMap::COL_DEBIT_USER, $debitUser['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(TransactionTableMap::COL_DEBIT_USER, $debitUser, $comparison);
    }

    /**
     * Filter the query on the credit_user column
     *
     * Example usage:
     * <code>
     * $query->filterByCreditUser(1234); // WHERE credit_user = 1234
     * $query->filterByCreditUser(array(12, 34)); // WHERE credit_user IN (12, 34)
     * $query->filterByCreditUser(array('min' => 12)); // WHERE credit_user > 12
     * </code>
     *
     * @see       filterByUserRelatedByCreditUser()
     *
     * @param     mixed $creditUser The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildTransactionQuery The current query, for fluid interface
     */
    public function filterByCreditUser($creditUser = null, $comparison = null)
    {
        if (is_array($creditUser)) {
            $useMinMax = false;
            if (isset($creditUser['min'])) {
                $this->addUsingAlias(TransactionTableMap::COL_CREDIT_USER, $creditUser['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($creditUser['max'])) {
                $this->addUsingAlias(TransactionTableMap::COL_CREDIT_USER, $creditUser['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(TransactionTableMap::COL_CREDIT_USER, $creditUser, $comparison);
    }

    /**
     * Filter the query on the amount column
     *
     * Example usage:
     * <code>
     * $query->filterByAmount('fooValue');   // WHERE amount = 'fooValue'
     * $query->filterByAmount('%fooValue%', Criteria::LIKE); // WHERE amount LIKE '%fooValue%'
     * </code>
     *
     * @param     string $amount The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildTransactionQuery The current query, for fluid interface
     */
    public function filterByAmount($amount = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($amount)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(TransactionTableMap::COL_AMOUNT, $amount, $comparison);
    }

    /**
     * Filter the query on the description column
     *
     * Example usage:
     * <code>
     * $query->filterByDescription('fooValue');   // WHERE description = 'fooValue'
     * $query->filterByDescription('%fooValue%', Criteria::LIKE); // WHERE description LIKE '%fooValue%'
     * </code>
     *
     * @param     string $description The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildTransactionQuery The current query, for fluid interface
     */
    public function filterByDescription($description = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($description)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(TransactionTableMap::COL_DESCRIPTION, $description, $comparison);
    }

    /**
     * Filter the query on the datecreated column
     *
     * Example usage:
     * <code>
     * $query->filterByDatecreated('2011-03-14'); // WHERE datecreated = '2011-03-14'
     * $query->filterByDatecreated('now'); // WHERE datecreated = '2011-03-14'
     * $query->filterByDatecreated(array('max' => 'yesterday')); // WHERE datecreated > '2011-03-13'
     * </code>
     *
     * @param     mixed $datecreated The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildTransactionQuery The current query, for fluid interface
     */
    public function filterByDatecreated($datecreated = null, $comparison = null)
    {
        if (is_array($datecreated)) {
            $useMinMax = false;
            if (isset($datecreated['min'])) {
                $this->addUsingAlias(TransactionTableMap::COL_DATECREATED, $datecreated['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($datecreated['max'])) {
                $this->addUsingAlias(TransactionTableMap::COL_DATECREATED, $datecreated['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(TransactionTableMap::COL_DATECREATED, $datecreated, $comparison);
    }

    /**
     * Filter the query on the lastmodified column
     *
     * Example usage:
     * <code>
     * $query->filterByLastmodified('2011-03-14'); // WHERE lastmodified = '2011-03-14'
     * $query->filterByLastmodified('now'); // WHERE lastmodified = '2011-03-14'
     * $query->filterByLastmodified(array('max' => 'yesterday')); // WHERE lastmodified > '2011-03-13'
     * </code>
     *
     * @param     mixed $lastmodified The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildTransactionQuery The current query, for fluid interface
     */
    public function filterByLastmodified($lastmodified = null, $comparison = null)
    {
        if (is_array($lastmodified)) {
            $useMinMax = false;
            if (isset($lastmodified['min'])) {
                $this->addUsingAlias(TransactionTableMap::COL_LASTMODIFIED, $lastmodified['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($lastmodified['max'])) {
                $this->addUsingAlias(TransactionTableMap::COL_LASTMODIFIED, $lastmodified['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(TransactionTableMap::COL_LASTMODIFIED, $lastmodified, $comparison);
    }

    /**
     * Filter the query on the advert_id column
     *
     * Example usage:
     * <code>
     * $query->filterByAdvertId(1234); // WHERE advert_id = 1234
     * $query->filterByAdvertId(array(12, 34)); // WHERE advert_id IN (12, 34)
     * $query->filterByAdvertId(array('min' => 12)); // WHERE advert_id > 12
     * </code>
     *
     * @see       filterByAdvert()
     *
     * @param     mixed $advertId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildTransactionQuery The current query, for fluid interface
     */
    public function filterByAdvertId($advertId = null, $comparison = null)
    {
        if (is_array($advertId)) {
            $useMinMax = false;
            if (isset($advertId['min'])) {
                $this->addUsingAlias(TransactionTableMap::COL_ADVERT_ID, $advertId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($advertId['max'])) {
                $this->addUsingAlias(TransactionTableMap::COL_ADVERT_ID, $advertId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(TransactionTableMap::COL_ADVERT_ID, $advertId, $comparison);
    }

    /**
     * Filter the query by a related \MoonlightAdminEntity\MoonlightAdminEntity\User object
     *
     * @param \MoonlightAdminEntity\MoonlightAdminEntity\User|ObjectCollection $user The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildTransactionQuery The current query, for fluid interface
     */
    public function filterByUserRelatedByDebitUser($user, $comparison = null)
    {
        if ($user instanceof \MoonlightAdminEntity\MoonlightAdminEntity\User) {
            return $this
                ->addUsingAlias(TransactionTableMap::COL_DEBIT_USER, $user->getId(), $comparison);
        } elseif ($user instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(TransactionTableMap::COL_DEBIT_USER, $user->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByUserRelatedByDebitUser() only accepts arguments of type \MoonlightAdminEntity\MoonlightAdminEntity\User or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the UserRelatedByDebitUser relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildTransactionQuery The current query, for fluid interface
     */
    public function joinUserRelatedByDebitUser($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('UserRelatedByDebitUser');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'UserRelatedByDebitUser');
        }

        return $this;
    }

    /**
     * Use the UserRelatedByDebitUser relation User object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \MoonlightAdminEntity\MoonlightAdminEntity\UserQuery A secondary query class using the current class as primary query
     */
    public function useUserRelatedByDebitUserQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinUserRelatedByDebitUser($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'UserRelatedByDebitUser', '\MoonlightAdminEntity\MoonlightAdminEntity\UserQuery');
    }

    /**
     * Filter the query by a related \MoonlightAdminEntity\MoonlightAdminEntity\User object
     *
     * @param \MoonlightAdminEntity\MoonlightAdminEntity\User|ObjectCollection $user The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildTransactionQuery The current query, for fluid interface
     */
    public function filterByUserRelatedByCreditUser($user, $comparison = null)
    {
        if ($user instanceof \MoonlightAdminEntity\MoonlightAdminEntity\User) {
            return $this
                ->addUsingAlias(TransactionTableMap::COL_CREDIT_USER, $user->getId(), $comparison);
        } elseif ($user instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(TransactionTableMap::COL_CREDIT_USER, $user->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByUserRelatedByCreditUser() only accepts arguments of type \MoonlightAdminEntity\MoonlightAdminEntity\User or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the UserRelatedByCreditUser relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildTransactionQuery The current query, for fluid interface
     */
    public function joinUserRelatedByCreditUser($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('UserRelatedByCreditUser');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'UserRelatedByCreditUser');
        }

        return $this;
    }

    /**
     * Use the UserRelatedByCreditUser relation User object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \MoonlightAdminEntity\MoonlightAdminEntity\UserQuery A secondary query class using the current class as primary query
     */
    public function useUserRelatedByCreditUserQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinUserRelatedByCreditUser($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'UserRelatedByCreditUser', '\MoonlightAdminEntity\MoonlightAdminEntity\UserQuery');
    }

    /**
     * Filter the query by a related \MoonlightAdminEntity\MoonlightAdminEntity\Advert object
     *
     * @param \MoonlightAdminEntity\MoonlightAdminEntity\Advert|ObjectCollection $advert The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildTransactionQuery The current query, for fluid interface
     */
    public function filterByAdvert($advert, $comparison = null)
    {
        if ($advert instanceof \MoonlightAdminEntity\MoonlightAdminEntity\Advert) {
            return $this
                ->addUsingAlias(TransactionTableMap::COL_ADVERT_ID, $advert->getId(), $comparison);
        } elseif ($advert instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(TransactionTableMap::COL_ADVERT_ID, $advert->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByAdvert() only accepts arguments of type \MoonlightAdminEntity\MoonlightAdminEntity\Advert or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Advert relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildTransactionQuery The current query, for fluid interface
     */
    public function joinAdvert($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Advert');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Advert');
        }

        return $this;
    }

    /**
     * Use the Advert relation Advert object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \MoonlightAdminEntity\MoonlightAdminEntity\AdvertQuery A secondary query class using the current class as primary query
     */
    public function useAdvertQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinAdvert($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Advert', '\MoonlightAdminEntity\MoonlightAdminEntity\AdvertQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildTransaction $transaction Object to remove from the list of results
     *
     * @return $this|ChildTransactionQuery The current query, for fluid interface
     */
    public function prune($transaction = null)
    {
        if ($transaction) {
            $this->addUsingAlias(TransactionTableMap::COL_ID, $transaction->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the transaction table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(TransactionTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            TransactionTableMap::clearInstancePool();
            TransactionTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(TransactionTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(TransactionTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            TransactionTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            TransactionTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // TransactionQuery
