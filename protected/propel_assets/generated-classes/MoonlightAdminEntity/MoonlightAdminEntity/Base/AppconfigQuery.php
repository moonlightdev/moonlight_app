<?php

namespace MoonlightAdminEntity\MoonlightAdminEntity\Base;

use \Exception;
use MoonlightAdminEntity\MoonlightAdminEntity\Appconfig as ChildAppconfig;
use MoonlightAdminEntity\MoonlightAdminEntity\AppconfigQuery as ChildAppconfigQuery;
use MoonlightAdminEntity\MoonlightAdminEntity\Map\AppconfigTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\LogicException;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'appconfig' table.
 *
 *
 *
 * @method     ChildAppconfigQuery orderByVersion($order = Criteria::ASC) Order by the version column
 *
 * @method     ChildAppconfigQuery groupByVersion() Group by the version column
 *
 * @method     ChildAppconfigQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildAppconfigQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildAppconfigQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildAppconfigQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildAppconfigQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildAppconfigQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildAppconfig findOne(ConnectionInterface $con = null) Return the first ChildAppconfig matching the query
 * @method     ChildAppconfig findOneOrCreate(ConnectionInterface $con = null) Return the first ChildAppconfig matching the query, or a new ChildAppconfig object populated from the query conditions when no match is found
 *
 * @method     ChildAppconfig findOneByVersion(int $version) Return the first ChildAppconfig filtered by the version column *

 * @method     ChildAppconfig requirePk($key, ConnectionInterface $con = null) Return the ChildAppconfig by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAppconfig requireOne(ConnectionInterface $con = null) Return the first ChildAppconfig matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildAppconfig requireOneByVersion(int $version) Return the first ChildAppconfig filtered by the version column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildAppconfig[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildAppconfig objects based on current ModelCriteria
 * @method     ChildAppconfig[]|ObjectCollection findByVersion(int $version) Return ChildAppconfig objects filtered by the version column
 * @method     ChildAppconfig[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class AppconfigQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \MoonlightAdminEntity\MoonlightAdminEntity\Base\AppconfigQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'default', $modelName = '\\MoonlightAdminEntity\\MoonlightAdminEntity\\Appconfig', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildAppconfigQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildAppconfigQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildAppconfigQuery) {
            return $criteria;
        }
        $query = new ChildAppconfigQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildAppconfig|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        throw new LogicException('The Appconfig object has no primary key');
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(array(12, 56), array(832, 123), array(123, 456)), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        throw new LogicException('The Appconfig object has no primary key');
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildAppconfigQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {
        throw new LogicException('The Appconfig object has no primary key');
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildAppconfigQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {
        throw new LogicException('The Appconfig object has no primary key');
    }

    /**
     * Filter the query on the version column
     *
     * Example usage:
     * <code>
     * $query->filterByVersion(1234); // WHERE version = 1234
     * $query->filterByVersion(array(12, 34)); // WHERE version IN (12, 34)
     * $query->filterByVersion(array('min' => 12)); // WHERE version > 12
     * </code>
     *
     * @param     mixed $version The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildAppconfigQuery The current query, for fluid interface
     */
    public function filterByVersion($version = null, $comparison = null)
    {
        if (is_array($version)) {
            $useMinMax = false;
            if (isset($version['min'])) {
                $this->addUsingAlias(AppconfigTableMap::COL_VERSION, $version['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($version['max'])) {
                $this->addUsingAlias(AppconfigTableMap::COL_VERSION, $version['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(AppconfigTableMap::COL_VERSION, $version, $comparison);
    }

    /**
     * Exclude object from result
     *
     * @param   ChildAppconfig $appconfig Object to remove from the list of results
     *
     * @return $this|ChildAppconfigQuery The current query, for fluid interface
     */
    public function prune($appconfig = null)
    {
        if ($appconfig) {
            throw new LogicException('Appconfig object has no primary key');

        }

        return $this;
    }

    /**
     * Deletes all rows from the appconfig table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(AppconfigTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            AppconfigTableMap::clearInstancePool();
            AppconfigTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(AppconfigTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(AppconfigTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            AppconfigTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            AppconfigTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // AppconfigQuery
