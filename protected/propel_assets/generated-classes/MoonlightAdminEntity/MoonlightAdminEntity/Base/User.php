<?php

namespace MoonlightAdminEntity\MoonlightAdminEntity\Base;

use \DateTime;
use \Exception;
use \PDO;
use MoonlightAdminEntity\MoonlightAdminEntity\Advert as ChildAdvert;
use MoonlightAdminEntity\MoonlightAdminEntity\AdvertQuery as ChildAdvertQuery;
use MoonlightAdminEntity\MoonlightAdminEntity\Transaction as ChildTransaction;
use MoonlightAdminEntity\MoonlightAdminEntity\TransactionQuery as ChildTransactionQuery;
use MoonlightAdminEntity\MoonlightAdminEntity\User as ChildUser;
use MoonlightAdminEntity\MoonlightAdminEntity\UserQuery as ChildUserQuery;
use MoonlightAdminEntity\MoonlightAdminEntity\Map\AdvertTableMap;
use MoonlightAdminEntity\MoonlightAdminEntity\Map\TransactionTableMap;
use MoonlightAdminEntity\MoonlightAdminEntity\Map\UserTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveRecord\ActiveRecordInterface;
use Propel\Runtime\Collection\Collection;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\BadMethodCallException;
use Propel\Runtime\Exception\LogicException;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Parser\AbstractParser;
use Propel\Runtime\Util\PropelDateTime;

/**
 * Base class that represents a row from the 'user' table.
 *
 *
 *
 * @package    propel.generator.MoonlightAdminEntity.MoonlightAdminEntity.Base
 */
abstract class User implements ActiveRecordInterface
{
    /**
     * TableMap class name
     */
    const TABLE_MAP = '\\MoonlightAdminEntity\\MoonlightAdminEntity\\Map\\UserTableMap';


    /**
     * attribute to determine if this object has previously been saved.
     * @var boolean
     */
    protected $new = true;

    /**
     * attribute to determine whether this object has been deleted.
     * @var boolean
     */
    protected $deleted = false;

    /**
     * The columns that have been modified in current object.
     * Tracking modified columns allows us to only update modified columns.
     * @var array
     */
    protected $modifiedColumns = array();

    /**
     * The (virtual) columns that are added at runtime
     * The formatters can add supplementary columns based on a resultset
     * @var array
     */
    protected $virtualColumns = array();

    /**
     * The value for the id field.
     *
     * @var        int
     */
    protected $id;

    /**
     * The value for the username field.
     *
     * @var        string
     */
    protected $username;

    /**
     * The value for the password field.
     *
     * @var        string
     */
    protected $password;

    /**
     * The value for the firstname field.
     *
     * @var        string
     */
    protected $firstname;

    /**
     * The value for the lastname field.
     *
     * @var        string
     */
    protected $lastname;

    /**
     * The value for the address field.
     *
     * @var        string
     */
    protected $address;

    /**
     * The value for the verified field.
     *
     * @var        boolean
     */
    protected $verified;

    /**
     * The value for the datecreated field.
     *
     * @var        DateTime
     */
    protected $datecreated;

    /**
     * The value for the lastmodified field.
     *
     * Note: this column has a database default value of: (expression) CURRENT_TIMESTAMP
     * @var        DateTime
     */
    protected $lastmodified;

    /**
     * @var        ObjectCollection|ChildAdvert[] Collection to store aggregation of ChildAdvert objects.
     */
    protected $collAdverts;
    protected $collAdvertsPartial;

    /**
     * @var        ObjectCollection|ChildTransaction[] Collection to store aggregation of ChildTransaction objects.
     */
    protected $collTransactionsRelatedByDebitUser;
    protected $collTransactionsRelatedByDebitUserPartial;

    /**
     * @var        ObjectCollection|ChildTransaction[] Collection to store aggregation of ChildTransaction objects.
     */
    protected $collTransactionsRelatedByCreditUser;
    protected $collTransactionsRelatedByCreditUserPartial;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     *
     * @var boolean
     */
    protected $alreadyInSave = false;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildAdvert[]
     */
    protected $advertsScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildTransaction[]
     */
    protected $transactionsRelatedByDebitUserScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildTransaction[]
     */
    protected $transactionsRelatedByCreditUserScheduledForDeletion = null;

    /**
     * Applies default values to this object.
     * This method should be called from the object's constructor (or
     * equivalent initialization method).
     * @see __construct()
     */
    public function applyDefaultValues()
    {
    }

    /**
     * Initializes internal state of MoonlightAdminEntity\MoonlightAdminEntity\Base\User object.
     * @see applyDefaults()
     */
    public function __construct()
    {
        $this->applyDefaultValues();
    }

    /**
     * Returns whether the object has been modified.
     *
     * @return boolean True if the object has been modified.
     */
    public function isModified()
    {
        return !!$this->modifiedColumns;
    }

    /**
     * Has specified column been modified?
     *
     * @param  string  $col column fully qualified name (TableMap::TYPE_COLNAME), e.g. Book::AUTHOR_ID
     * @return boolean True if $col has been modified.
     */
    public function isColumnModified($col)
    {
        return $this->modifiedColumns && isset($this->modifiedColumns[$col]);
    }

    /**
     * Get the columns that have been modified in this object.
     * @return array A unique list of the modified column names for this object.
     */
    public function getModifiedColumns()
    {
        return $this->modifiedColumns ? array_keys($this->modifiedColumns) : [];
    }

    /**
     * Returns whether the object has ever been saved.  This will
     * be false, if the object was retrieved from storage or was created
     * and then saved.
     *
     * @return boolean true, if the object has never been persisted.
     */
    public function isNew()
    {
        return $this->new;
    }

    /**
     * Setter for the isNew attribute.  This method will be called
     * by Propel-generated children and objects.
     *
     * @param boolean $b the state of the object.
     */
    public function setNew($b)
    {
        $this->new = (boolean) $b;
    }

    /**
     * Whether this object has been deleted.
     * @return boolean The deleted state of this object.
     */
    public function isDeleted()
    {
        return $this->deleted;
    }

    /**
     * Specify whether this object has been deleted.
     * @param  boolean $b The deleted state of this object.
     * @return void
     */
    public function setDeleted($b)
    {
        $this->deleted = (boolean) $b;
    }

    /**
     * Sets the modified state for the object to be false.
     * @param  string $col If supplied, only the specified column is reset.
     * @return void
     */
    public function resetModified($col = null)
    {
        if (null !== $col) {
            if (isset($this->modifiedColumns[$col])) {
                unset($this->modifiedColumns[$col]);
            }
        } else {
            $this->modifiedColumns = array();
        }
    }

    /**
     * Compares this with another <code>User</code> instance.  If
     * <code>obj</code> is an instance of <code>User</code>, delegates to
     * <code>equals(User)</code>.  Otherwise, returns <code>false</code>.
     *
     * @param  mixed   $obj The object to compare to.
     * @return boolean Whether equal to the object specified.
     */
    public function equals($obj)
    {
        if (!$obj instanceof static) {
            return false;
        }

        if ($this === $obj) {
            return true;
        }

        if (null === $this->getPrimaryKey() || null === $obj->getPrimaryKey()) {
            return false;
        }

        return $this->getPrimaryKey() === $obj->getPrimaryKey();
    }

    /**
     * Get the associative array of the virtual columns in this object
     *
     * @return array
     */
    public function getVirtualColumns()
    {
        return $this->virtualColumns;
    }

    /**
     * Checks the existence of a virtual column in this object
     *
     * @param  string  $name The virtual column name
     * @return boolean
     */
    public function hasVirtualColumn($name)
    {
        return array_key_exists($name, $this->virtualColumns);
    }

    /**
     * Get the value of a virtual column in this object
     *
     * @param  string $name The virtual column name
     * @return mixed
     *
     * @throws PropelException
     */
    public function getVirtualColumn($name)
    {
        if (!$this->hasVirtualColumn($name)) {
            throw new PropelException(sprintf('Cannot get value of inexistent virtual column %s.', $name));
        }

        return $this->virtualColumns[$name];
    }

    /**
     * Set the value of a virtual column in this object
     *
     * @param string $name  The virtual column name
     * @param mixed  $value The value to give to the virtual column
     *
     * @return $this|User The current object, for fluid interface
     */
    public function setVirtualColumn($name, $value)
    {
        $this->virtualColumns[$name] = $value;

        return $this;
    }

    /**
     * Logs a message using Propel::log().
     *
     * @param  string  $msg
     * @param  int     $priority One of the Propel::LOG_* logging levels
     * @return boolean
     */
    protected function log($msg, $priority = Propel::LOG_INFO)
    {
        return Propel::log(get_class($this) . ': ' . $msg, $priority);
    }

    /**
     * Export the current object properties to a string, using a given parser format
     * <code>
     * $book = BookQuery::create()->findPk(9012);
     * echo $book->exportTo('JSON');
     *  => {"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * @param  mixed   $parser                 A AbstractParser instance, or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param  boolean $includeLazyLoadColumns (optional) Whether to include lazy load(ed) columns. Defaults to TRUE.
     * @return string  The exported data
     */
    public function exportTo($parser, $includeLazyLoadColumns = true)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        return $parser->fromArray($this->toArray(TableMap::TYPE_PHPNAME, $includeLazyLoadColumns, array(), true));
    }

    /**
     * Clean up internal collections prior to serializing
     * Avoids recursive loops that turn into segmentation faults when serializing
     */
    public function __sleep()
    {
        $this->clearAllReferences();

        $cls = new \ReflectionClass($this);
        $propertyNames = [];
        $serializableProperties = array_diff($cls->getProperties(), $cls->getProperties(\ReflectionProperty::IS_STATIC));

        foreach($serializableProperties as $property) {
            $propertyNames[] = $property->getName();
        }

        return $propertyNames;
    }

    /**
     * Get the [id] column value.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get the [username] column value.
     *
     * @return string
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * Get the [password] column value.
     *
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * Get the [firstname] column value.
     *
     * @return string
     */
    public function getFirstname()
    {
        return $this->firstname;
    }

    /**
     * Get the [lastname] column value.
     *
     * @return string
     */
    public function getLastname()
    {
        return $this->lastname;
    }

    /**
     * Get the [address] column value.
     *
     * @return string
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * Get the [verified] column value.
     *
     * @return boolean
     */
    public function getVerified()
    {
        return $this->verified;
    }

    /**
     * Get the [verified] column value.
     *
     * @return boolean
     */
    public function isVerified()
    {
        return $this->getVerified();
    }

    /**
     * Get the [optionally formatted] temporal [datecreated] column value.
     *
     *
     * @param      string $format The date/time format string (either date()-style or strftime()-style).
     *                            If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00 00:00:00
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getDatecreated($format = NULL)
    {
        if ($format === null) {
            return $this->datecreated;
        } else {
            return $this->datecreated instanceof \DateTimeInterface ? $this->datecreated->format($format) : null;
        }
    }

    /**
     * Get the [optionally formatted] temporal [lastmodified] column value.
     *
     *
     * @param      string $format The date/time format string (either date()-style or strftime()-style).
     *                            If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00 00:00:00
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getLastmodified($format = NULL)
    {
        if ($format === null) {
            return $this->lastmodified;
        } else {
            return $this->lastmodified instanceof \DateTimeInterface ? $this->lastmodified->format($format) : null;
        }
    }

    /**
     * Set the value of [id] column.
     *
     * @param int $v new value
     * @return $this|\MoonlightAdminEntity\MoonlightAdminEntity\User The current object (for fluent API support)
     */
    public function setId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->id !== $v) {
            $this->id = $v;
            $this->modifiedColumns[UserTableMap::COL_ID] = true;
        }

        return $this;
    } // setId()

    /**
     * Set the value of [username] column.
     *
     * @param string $v new value
     * @return $this|\MoonlightAdminEntity\MoonlightAdminEntity\User The current object (for fluent API support)
     */
    public function setUsername($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->username !== $v) {
            $this->username = $v;
            $this->modifiedColumns[UserTableMap::COL_USERNAME] = true;
        }

        return $this;
    } // setUsername()

    /**
     * Set the value of [password] column.
     *
     * @param string $v new value
     * @return $this|\MoonlightAdminEntity\MoonlightAdminEntity\User The current object (for fluent API support)
     */
    public function setPassword($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->password !== $v) {
            $this->password = $v;
            $this->modifiedColumns[UserTableMap::COL_PASSWORD] = true;
        }

        return $this;
    } // setPassword()

    /**
     * Set the value of [firstname] column.
     *
     * @param string $v new value
     * @return $this|\MoonlightAdminEntity\MoonlightAdminEntity\User The current object (for fluent API support)
     */
    public function setFirstname($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->firstname !== $v) {
            $this->firstname = $v;
            $this->modifiedColumns[UserTableMap::COL_FIRSTNAME] = true;
        }

        return $this;
    } // setFirstname()

    /**
     * Set the value of [lastname] column.
     *
     * @param string $v new value
     * @return $this|\MoonlightAdminEntity\MoonlightAdminEntity\User The current object (for fluent API support)
     */
    public function setLastname($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->lastname !== $v) {
            $this->lastname = $v;
            $this->modifiedColumns[UserTableMap::COL_LASTNAME] = true;
        }

        return $this;
    } // setLastname()

    /**
     * Set the value of [address] column.
     *
     * @param string $v new value
     * @return $this|\MoonlightAdminEntity\MoonlightAdminEntity\User The current object (for fluent API support)
     */
    public function setAddress($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->address !== $v) {
            $this->address = $v;
            $this->modifiedColumns[UserTableMap::COL_ADDRESS] = true;
        }

        return $this;
    } // setAddress()

    /**
     * Sets the value of the [verified] column.
     * Non-boolean arguments are converted using the following rules:
     *   * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *   * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     * Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     *
     * @param  boolean|integer|string $v The new value
     * @return $this|\MoonlightAdminEntity\MoonlightAdminEntity\User The current object (for fluent API support)
     */
    public function setVerified($v)
    {
        if ($v !== null) {
            if (is_string($v)) {
                $v = in_array(strtolower($v), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
            } else {
                $v = (boolean) $v;
            }
        }

        if ($this->verified !== $v) {
            $this->verified = $v;
            $this->modifiedColumns[UserTableMap::COL_VERIFIED] = true;
        }

        return $this;
    } // setVerified()

    /**
     * Sets the value of [datecreated] column to a normalized version of the date/time value specified.
     *
     * @param  mixed $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this|\MoonlightAdminEntity\MoonlightAdminEntity\User The current object (for fluent API support)
     */
    public function setDatecreated($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->datecreated !== null || $dt !== null) {
            if ($this->datecreated === null || $dt === null || $dt->format("Y-m-d H:i:s.u") !== $this->datecreated->format("Y-m-d H:i:s.u")) {
                $this->datecreated = $dt === null ? null : clone $dt;
                $this->modifiedColumns[UserTableMap::COL_DATECREATED] = true;
            }
        } // if either are not null

        return $this;
    } // setDatecreated()

    /**
     * Sets the value of [lastmodified] column to a normalized version of the date/time value specified.
     *
     * @param  mixed $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this|\MoonlightAdminEntity\MoonlightAdminEntity\User The current object (for fluent API support)
     */
    public function setLastmodified($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->lastmodified !== null || $dt !== null) {
            if ($this->lastmodified === null || $dt === null || $dt->format("Y-m-d H:i:s.u") !== $this->lastmodified->format("Y-m-d H:i:s.u")) {
                $this->lastmodified = $dt === null ? null : clone $dt;
                $this->modifiedColumns[UserTableMap::COL_LASTMODIFIED] = true;
            }
        } // if either are not null

        return $this;
    } // setLastmodified()

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return boolean Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues()
    {
        // otherwise, everything was equal, so return TRUE
        return true;
    } // hasOnlyDefaultValues()

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array   $row       The row returned by DataFetcher->fetch().
     * @param int     $startcol  0-based offset column which indicates which restultset column to start with.
     * @param boolean $rehydrate Whether this object is being re-hydrated from the database.
     * @param string  $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                  One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                            TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @return int             next starting column
     * @throws PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate($row, $startcol = 0, $rehydrate = false, $indexType = TableMap::TYPE_NUM)
    {
        try {

            $col = $row[TableMap::TYPE_NUM == $indexType ? 0 + $startcol : UserTableMap::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)];
            $this->id = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 1 + $startcol : UserTableMap::translateFieldName('Username', TableMap::TYPE_PHPNAME, $indexType)];
            $this->username = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 2 + $startcol : UserTableMap::translateFieldName('Password', TableMap::TYPE_PHPNAME, $indexType)];
            $this->password = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 3 + $startcol : UserTableMap::translateFieldName('Firstname', TableMap::TYPE_PHPNAME, $indexType)];
            $this->firstname = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 4 + $startcol : UserTableMap::translateFieldName('Lastname', TableMap::TYPE_PHPNAME, $indexType)];
            $this->lastname = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 5 + $startcol : UserTableMap::translateFieldName('Address', TableMap::TYPE_PHPNAME, $indexType)];
            $this->address = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 6 + $startcol : UserTableMap::translateFieldName('Verified', TableMap::TYPE_PHPNAME, $indexType)];
            $this->verified = (null !== $col) ? (boolean) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 7 + $startcol : UserTableMap::translateFieldName('Datecreated', TableMap::TYPE_PHPNAME, $indexType)];
            if ($col === '0000-00-00 00:00:00') {
                $col = null;
            }
            $this->datecreated = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 8 + $startcol : UserTableMap::translateFieldName('Lastmodified', TableMap::TYPE_PHPNAME, $indexType)];
            if ($col === '0000-00-00 00:00:00') {
                $col = null;
            }
            $this->lastmodified = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }

            return $startcol + 9; // 9 = UserTableMap::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException(sprintf('Error populating %s object', '\\MoonlightAdminEntity\\MoonlightAdminEntity\\User'), 0, $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws PropelException
     */
    public function ensureConsistency()
    {
    } // ensureConsistency

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param      boolean $deep (optional) Whether to also de-associated any related objects.
     * @param      ConnectionInterface $con (optional) The ConnectionInterface connection to use.
     * @return void
     * @throws PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload($deep = false, ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(UserTableMap::DATABASE_NAME);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $dataFetcher = ChildUserQuery::create(null, $this->buildPkeyCriteria())->setFormatter(ModelCriteria::FORMAT_STATEMENT)->find($con);
        $row = $dataFetcher->fetch();
        $dataFetcher->close();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true, $dataFetcher->getIndexType()); // rehydrate

        if ($deep) {  // also de-associate any related objects?

            $this->collAdverts = null;

            $this->collTransactionsRelatedByDebitUser = null;

            $this->collTransactionsRelatedByCreditUser = null;

        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param      ConnectionInterface $con
     * @return void
     * @throws PropelException
     * @see User::setDeleted()
     * @see User::isDeleted()
     */
    public function delete(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(UserTableMap::DATABASE_NAME);
        }

        $con->transaction(function () use ($con) {
            $deleteQuery = ChildUserQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $this->setDeleted(true);
            }
        });
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see doSave()
     */
    public function save(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($this->alreadyInSave) {
            return 0;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(UserTableMap::DATABASE_NAME);
        }

        return $con->transaction(function () use ($con) {
            $ret = $this->preSave($con);
            $isInsert = $this->isNew();
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
            } else {
                $ret = $ret && $this->preUpdate($con);
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                UserTableMap::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }

            return $affectedRows;
        });
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see save()
     */
    protected function doSave(ConnectionInterface $con)
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                    $affectedRows += 1;
                } else {
                    $affectedRows += $this->doUpdate($con);
                }
                $this->resetModified();
            }

            if ($this->advertsScheduledForDeletion !== null) {
                if (!$this->advertsScheduledForDeletion->isEmpty()) {
                    \MoonlightAdminEntity\MoonlightAdminEntity\AdvertQuery::create()
                        ->filterByPrimaryKeys($this->advertsScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->advertsScheduledForDeletion = null;
                }
            }

            if ($this->collAdverts !== null) {
                foreach ($this->collAdverts as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->transactionsRelatedByDebitUserScheduledForDeletion !== null) {
                if (!$this->transactionsRelatedByDebitUserScheduledForDeletion->isEmpty()) {
                    \MoonlightAdminEntity\MoonlightAdminEntity\TransactionQuery::create()
                        ->filterByPrimaryKeys($this->transactionsRelatedByDebitUserScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->transactionsRelatedByDebitUserScheduledForDeletion = null;
                }
            }

            if ($this->collTransactionsRelatedByDebitUser !== null) {
                foreach ($this->collTransactionsRelatedByDebitUser as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->transactionsRelatedByCreditUserScheduledForDeletion !== null) {
                if (!$this->transactionsRelatedByCreditUserScheduledForDeletion->isEmpty()) {
                    \MoonlightAdminEntity\MoonlightAdminEntity\TransactionQuery::create()
                        ->filterByPrimaryKeys($this->transactionsRelatedByCreditUserScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->transactionsRelatedByCreditUserScheduledForDeletion = null;
                }
            }

            if ($this->collTransactionsRelatedByCreditUser !== null) {
                foreach ($this->collTransactionsRelatedByCreditUser as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    } // doSave()

    /**
     * Insert the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @throws PropelException
     * @see doSave()
     */
    protected function doInsert(ConnectionInterface $con)
    {
        $modifiedColumns = array();
        $index = 0;

        $this->modifiedColumns[UserTableMap::COL_ID] = true;
        if (null !== $this->id) {
            throw new PropelException('Cannot insert a value for auto-increment primary key (' . UserTableMap::COL_ID . ')');
        }

         // check the columns in natural order for more readable SQL queries
        if ($this->isColumnModified(UserTableMap::COL_ID)) {
            $modifiedColumns[':p' . $index++]  = 'id';
        }
        if ($this->isColumnModified(UserTableMap::COL_USERNAME)) {
            $modifiedColumns[':p' . $index++]  = 'username';
        }
        if ($this->isColumnModified(UserTableMap::COL_PASSWORD)) {
            $modifiedColumns[':p' . $index++]  = 'password';
        }
        if ($this->isColumnModified(UserTableMap::COL_FIRSTNAME)) {
            $modifiedColumns[':p' . $index++]  = 'firstname';
        }
        if ($this->isColumnModified(UserTableMap::COL_LASTNAME)) {
            $modifiedColumns[':p' . $index++]  = 'lastname';
        }
        if ($this->isColumnModified(UserTableMap::COL_ADDRESS)) {
            $modifiedColumns[':p' . $index++]  = 'address';
        }
        if ($this->isColumnModified(UserTableMap::COL_VERIFIED)) {
            $modifiedColumns[':p' . $index++]  = 'verified';
        }
        if ($this->isColumnModified(UserTableMap::COL_DATECREATED)) {
            $modifiedColumns[':p' . $index++]  = 'datecreated';
        }
        if ($this->isColumnModified(UserTableMap::COL_LASTMODIFIED)) {
            $modifiedColumns[':p' . $index++]  = 'lastmodified';
        }

        $sql = sprintf(
            'INSERT INTO user (%s) VALUES (%s)',
            implode(', ', $modifiedColumns),
            implode(', ', array_keys($modifiedColumns))
        );

        try {
            $stmt = $con->prepare($sql);
            foreach ($modifiedColumns as $identifier => $columnName) {
                switch ($columnName) {
                    case 'id':
                        $stmt->bindValue($identifier, $this->id, PDO::PARAM_INT);
                        break;
                    case 'username':
                        $stmt->bindValue($identifier, $this->username, PDO::PARAM_STR);
                        break;
                    case 'password':
                        $stmt->bindValue($identifier, $this->password, PDO::PARAM_STR);
                        break;
                    case 'firstname':
                        $stmt->bindValue($identifier, $this->firstname, PDO::PARAM_STR);
                        break;
                    case 'lastname':
                        $stmt->bindValue($identifier, $this->lastname, PDO::PARAM_STR);
                        break;
                    case 'address':
                        $stmt->bindValue($identifier, $this->address, PDO::PARAM_STR);
                        break;
                    case 'verified':
                        $stmt->bindValue($identifier, (int) $this->verified, PDO::PARAM_INT);
                        break;
                    case 'datecreated':
                        $stmt->bindValue($identifier, $this->datecreated ? $this->datecreated->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                    case 'lastmodified':
                        $stmt->bindValue($identifier, $this->lastmodified ? $this->lastmodified->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                }
            }
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute INSERT statement [%s]', $sql), 0, $e);
        }

        try {
            $pk = $con->lastInsertId();
        } catch (Exception $e) {
            throw new PropelException('Unable to get autoincrement id.', 0, $e);
        }
        $this->setId($pk);

        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @return Integer Number of updated rows
     * @see doSave()
     */
    protected function doUpdate(ConnectionInterface $con)
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();

        return $selectCriteria->doUpdate($valuesCriteria, $con);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param      string $name name
     * @param      string $type The type of fieldname the $name is of:
     *                     one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                     TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                     Defaults to TableMap::TYPE_PHPNAME.
     * @return mixed Value of field.
     */
    public function getByName($name, $type = TableMap::TYPE_PHPNAME)
    {
        $pos = UserTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param      int $pos position in xml schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition($pos)
    {
        switch ($pos) {
            case 0:
                return $this->getId();
                break;
            case 1:
                return $this->getUsername();
                break;
            case 2:
                return $this->getPassword();
                break;
            case 3:
                return $this->getFirstname();
                break;
            case 4:
                return $this->getLastname();
                break;
            case 5:
                return $this->getAddress();
                break;
            case 6:
                return $this->getVerified();
                break;
            case 7:
                return $this->getDatecreated();
                break;
            case 8:
                return $this->getLastmodified();
                break;
            default:
                return null;
                break;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param     string  $keyType (optional) One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     *                    TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                    Defaults to TableMap::TYPE_PHPNAME.
     * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to TRUE.
     * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
     * @param     boolean $includeForeignObjects (optional) Whether to include hydrated related objects. Default to FALSE.
     *
     * @return array an associative array containing the field names (as keys) and field values
     */
    public function toArray($keyType = TableMap::TYPE_PHPNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array(), $includeForeignObjects = false)
    {

        if (isset($alreadyDumpedObjects['User'][$this->hashCode()])) {
            return '*RECURSION*';
        }
        $alreadyDumpedObjects['User'][$this->hashCode()] = true;
        $keys = UserTableMap::getFieldNames($keyType);
        $result = array(
            $keys[0] => $this->getId(),
            $keys[1] => $this->getUsername(),
            $keys[2] => $this->getPassword(),
            $keys[3] => $this->getFirstname(),
            $keys[4] => $this->getLastname(),
            $keys[5] => $this->getAddress(),
            $keys[6] => $this->getVerified(),
            $keys[7] => $this->getDatecreated(),
            $keys[8] => $this->getLastmodified(),
        );
        if ($result[$keys[7]] instanceof \DateTimeInterface) {
            $result[$keys[7]] = $result[$keys[7]]->format('c');
        }

        if ($result[$keys[8]] instanceof \DateTimeInterface) {
            $result[$keys[8]] = $result[$keys[8]]->format('c');
        }

        $virtualColumns = $this->virtualColumns;
        foreach ($virtualColumns as $key => $virtualColumn) {
            $result[$key] = $virtualColumn;
        }

        if ($includeForeignObjects) {
            if (null !== $this->collAdverts) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'adverts';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'adverts';
                        break;
                    default:
                        $key = 'Adverts';
                }

                $result[$key] = $this->collAdverts->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collTransactionsRelatedByDebitUser) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'transactions';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'transactions';
                        break;
                    default:
                        $key = 'Transactions';
                }

                $result[$key] = $this->collTransactionsRelatedByDebitUser->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collTransactionsRelatedByCreditUser) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'transactions';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'transactions';
                        break;
                    default:
                        $key = 'Transactions';
                }

                $result[$key] = $this->collTransactionsRelatedByCreditUser->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
        }

        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param  string $name
     * @param  mixed  $value field value
     * @param  string $type The type of fieldname the $name is of:
     *                one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                Defaults to TableMap::TYPE_PHPNAME.
     * @return $this|\MoonlightAdminEntity\MoonlightAdminEntity\User
     */
    public function setByName($name, $value, $type = TableMap::TYPE_PHPNAME)
    {
        $pos = UserTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);

        return $this->setByPosition($pos, $value);
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param  int $pos position in xml schema
     * @param  mixed $value field value
     * @return $this|\MoonlightAdminEntity\MoonlightAdminEntity\User
     */
    public function setByPosition($pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setId($value);
                break;
            case 1:
                $this->setUsername($value);
                break;
            case 2:
                $this->setPassword($value);
                break;
            case 3:
                $this->setFirstname($value);
                break;
            case 4:
                $this->setLastname($value);
                break;
            case 5:
                $this->setAddress($value);
                break;
            case 6:
                $this->setVerified($value);
                break;
            case 7:
                $this->setDatecreated($value);
                break;
            case 8:
                $this->setLastmodified($value);
                break;
        } // switch()

        return $this;
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_PHPNAME.
     *
     * @param      array  $arr     An array to populate the object from.
     * @param      string $keyType The type of keys the array uses.
     * @return void
     */
    public function fromArray($arr, $keyType = TableMap::TYPE_PHPNAME)
    {
        $keys = UserTableMap::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) {
            $this->setId($arr[$keys[0]]);
        }
        if (array_key_exists($keys[1], $arr)) {
            $this->setUsername($arr[$keys[1]]);
        }
        if (array_key_exists($keys[2], $arr)) {
            $this->setPassword($arr[$keys[2]]);
        }
        if (array_key_exists($keys[3], $arr)) {
            $this->setFirstname($arr[$keys[3]]);
        }
        if (array_key_exists($keys[4], $arr)) {
            $this->setLastname($arr[$keys[4]]);
        }
        if (array_key_exists($keys[5], $arr)) {
            $this->setAddress($arr[$keys[5]]);
        }
        if (array_key_exists($keys[6], $arr)) {
            $this->setVerified($arr[$keys[6]]);
        }
        if (array_key_exists($keys[7], $arr)) {
            $this->setDatecreated($arr[$keys[7]]);
        }
        if (array_key_exists($keys[8], $arr)) {
            $this->setLastmodified($arr[$keys[8]]);
        }
    }

     /**
     * Populate the current object from a string, using a given parser format
     * <code>
     * $book = new Book();
     * $book->importFrom('JSON', '{"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_PHPNAME.
     *
     * @param mixed $parser A AbstractParser instance,
     *                       or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param string $data The source data to import from
     * @param string $keyType The type of keys the array uses.
     *
     * @return $this|\MoonlightAdminEntity\MoonlightAdminEntity\User The current object, for fluid interface
     */
    public function importFrom($parser, $data, $keyType = TableMap::TYPE_PHPNAME)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        $this->fromArray($parser->toArray($data), $keyType);

        return $this;
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria()
    {
        $criteria = new Criteria(UserTableMap::DATABASE_NAME);

        if ($this->isColumnModified(UserTableMap::COL_ID)) {
            $criteria->add(UserTableMap::COL_ID, $this->id);
        }
        if ($this->isColumnModified(UserTableMap::COL_USERNAME)) {
            $criteria->add(UserTableMap::COL_USERNAME, $this->username);
        }
        if ($this->isColumnModified(UserTableMap::COL_PASSWORD)) {
            $criteria->add(UserTableMap::COL_PASSWORD, $this->password);
        }
        if ($this->isColumnModified(UserTableMap::COL_FIRSTNAME)) {
            $criteria->add(UserTableMap::COL_FIRSTNAME, $this->firstname);
        }
        if ($this->isColumnModified(UserTableMap::COL_LASTNAME)) {
            $criteria->add(UserTableMap::COL_LASTNAME, $this->lastname);
        }
        if ($this->isColumnModified(UserTableMap::COL_ADDRESS)) {
            $criteria->add(UserTableMap::COL_ADDRESS, $this->address);
        }
        if ($this->isColumnModified(UserTableMap::COL_VERIFIED)) {
            $criteria->add(UserTableMap::COL_VERIFIED, $this->verified);
        }
        if ($this->isColumnModified(UserTableMap::COL_DATECREATED)) {
            $criteria->add(UserTableMap::COL_DATECREATED, $this->datecreated);
        }
        if ($this->isColumnModified(UserTableMap::COL_LASTMODIFIED)) {
            $criteria->add(UserTableMap::COL_LASTMODIFIED, $this->lastmodified);
        }

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether or not they have been modified.
     *
     * @throws LogicException if no primary key is defined
     *
     * @return Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria()
    {
        $criteria = ChildUserQuery::create();
        $criteria->add(UserTableMap::COL_ID, $this->id);

        return $criteria;
    }

    /**
     * If the primary key is not null, return the hashcode of the
     * primary key. Otherwise, return the hash code of the object.
     *
     * @return int Hashcode
     */
    public function hashCode()
    {
        $validPk = null !== $this->getId();

        $validPrimaryKeyFKs = 0;
        $primaryKeyFKs = [];

        if ($validPk) {
            return crc32(json_encode($this->getPrimaryKey(), JSON_UNESCAPED_UNICODE));
        } elseif ($validPrimaryKeyFKs) {
            return crc32(json_encode($primaryKeyFKs, JSON_UNESCAPED_UNICODE));
        }

        return spl_object_hash($this);
    }

    /**
     * Returns the primary key for this object (row).
     * @return int
     */
    public function getPrimaryKey()
    {
        return $this->getId();
    }

    /**
     * Generic method to set the primary key (id column).
     *
     * @param       int $key Primary key.
     * @return void
     */
    public function setPrimaryKey($key)
    {
        $this->setId($key);
    }

    /**
     * Returns true if the primary key for this object is null.
     * @return boolean
     */
    public function isPrimaryKeyNull()
    {
        return null === $this->getId();
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param      object $copyObj An object of \MoonlightAdminEntity\MoonlightAdminEntity\User (or compatible) type.
     * @param      boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param      boolean $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws PropelException
     */
    public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
    {
        $copyObj->setUsername($this->getUsername());
        $copyObj->setPassword($this->getPassword());
        $copyObj->setFirstname($this->getFirstname());
        $copyObj->setLastname($this->getLastname());
        $copyObj->setAddress($this->getAddress());
        $copyObj->setVerified($this->getVerified());
        $copyObj->setDatecreated($this->getDatecreated());
        $copyObj->setLastmodified($this->getLastmodified());

        if ($deepCopy) {
            // important: temporarily setNew(false) because this affects the behavior of
            // the getter/setter methods for fkey referrer objects.
            $copyObj->setNew(false);

            foreach ($this->getAdverts() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addAdvert($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getTransactionsRelatedByDebitUser() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addTransactionRelatedByDebitUser($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getTransactionsRelatedByCreditUser() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addTransactionRelatedByCreditUser($relObj->copy($deepCopy));
                }
            }

        } // if ($deepCopy)

        if ($makeNew) {
            $copyObj->setNew(true);
            $copyObj->setId(NULL); // this is a auto-increment column, so set to default value
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param  boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return \MoonlightAdminEntity\MoonlightAdminEntity\User Clone of current object.
     * @throws PropelException
     */
    public function copy($deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }


    /**
     * Initializes a collection based on the name of a relation.
     * Avoids crafting an 'init[$relationName]s' method name
     * that wouldn't work when StandardEnglishPluralizer is used.
     *
     * @param      string $relationName The name of the relation to initialize
     * @return void
     */
    public function initRelation($relationName)
    {
        if ('Advert' == $relationName) {
            $this->initAdverts();
            return;
        }
        if ('TransactionRelatedByDebitUser' == $relationName) {
            $this->initTransactionsRelatedByDebitUser();
            return;
        }
        if ('TransactionRelatedByCreditUser' == $relationName) {
            $this->initTransactionsRelatedByCreditUser();
            return;
        }
    }

    /**
     * Clears out the collAdverts collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addAdverts()
     */
    public function clearAdverts()
    {
        $this->collAdverts = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collAdverts collection loaded partially.
     */
    public function resetPartialAdverts($v = true)
    {
        $this->collAdvertsPartial = $v;
    }

    /**
     * Initializes the collAdverts collection.
     *
     * By default this just sets the collAdverts collection to an empty array (like clearcollAdverts());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initAdverts($overrideExisting = true)
    {
        if (null !== $this->collAdverts && !$overrideExisting) {
            return;
        }

        $collectionClassName = AdvertTableMap::getTableMap()->getCollectionClassName();

        $this->collAdverts = new $collectionClassName;
        $this->collAdverts->setModel('\MoonlightAdminEntity\MoonlightAdminEntity\Advert');
    }

    /**
     * Gets an array of ChildAdvert objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildUser is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildAdvert[] List of ChildAdvert objects
     * @throws PropelException
     */
    public function getAdverts(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collAdvertsPartial && !$this->isNew();
        if (null === $this->collAdverts || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collAdverts) {
                // return empty collection
                $this->initAdverts();
            } else {
                $collAdverts = ChildAdvertQuery::create(null, $criteria)
                    ->filterByUser($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collAdvertsPartial && count($collAdverts)) {
                        $this->initAdverts(false);

                        foreach ($collAdverts as $obj) {
                            if (false == $this->collAdverts->contains($obj)) {
                                $this->collAdverts->append($obj);
                            }
                        }

                        $this->collAdvertsPartial = true;
                    }

                    return $collAdverts;
                }

                if ($partial && $this->collAdverts) {
                    foreach ($this->collAdverts as $obj) {
                        if ($obj->isNew()) {
                            $collAdverts[] = $obj;
                        }
                    }
                }

                $this->collAdverts = $collAdverts;
                $this->collAdvertsPartial = false;
            }
        }

        return $this->collAdverts;
    }

    /**
     * Sets a collection of ChildAdvert objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $adverts A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildUser The current object (for fluent API support)
     */
    public function setAdverts(Collection $adverts, ConnectionInterface $con = null)
    {
        /** @var ChildAdvert[] $advertsToDelete */
        $advertsToDelete = $this->getAdverts(new Criteria(), $con)->diff($adverts);


        $this->advertsScheduledForDeletion = $advertsToDelete;

        foreach ($advertsToDelete as $advertRemoved) {
            $advertRemoved->setUser(null);
        }

        $this->collAdverts = null;
        foreach ($adverts as $advert) {
            $this->addAdvert($advert);
        }

        $this->collAdverts = $adverts;
        $this->collAdvertsPartial = false;

        return $this;
    }

    /**
     * Returns the number of related Advert objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related Advert objects.
     * @throws PropelException
     */
    public function countAdverts(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collAdvertsPartial && !$this->isNew();
        if (null === $this->collAdverts || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collAdverts) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getAdverts());
            }

            $query = ChildAdvertQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByUser($this)
                ->count($con);
        }

        return count($this->collAdverts);
    }

    /**
     * Method called to associate a ChildAdvert object to this object
     * through the ChildAdvert foreign key attribute.
     *
     * @param  ChildAdvert $l ChildAdvert
     * @return $this|\MoonlightAdminEntity\MoonlightAdminEntity\User The current object (for fluent API support)
     */
    public function addAdvert(ChildAdvert $l)
    {
        if ($this->collAdverts === null) {
            $this->initAdverts();
            $this->collAdvertsPartial = true;
        }

        if (!$this->collAdverts->contains($l)) {
            $this->doAddAdvert($l);

            if ($this->advertsScheduledForDeletion and $this->advertsScheduledForDeletion->contains($l)) {
                $this->advertsScheduledForDeletion->remove($this->advertsScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildAdvert $advert The ChildAdvert object to add.
     */
    protected function doAddAdvert(ChildAdvert $advert)
    {
        $this->collAdverts[]= $advert;
        $advert->setUser($this);
    }

    /**
     * @param  ChildAdvert $advert The ChildAdvert object to remove.
     * @return $this|ChildUser The current object (for fluent API support)
     */
    public function removeAdvert(ChildAdvert $advert)
    {
        if ($this->getAdverts()->contains($advert)) {
            $pos = $this->collAdverts->search($advert);
            $this->collAdverts->remove($pos);
            if (null === $this->advertsScheduledForDeletion) {
                $this->advertsScheduledForDeletion = clone $this->collAdverts;
                $this->advertsScheduledForDeletion->clear();
            }
            $this->advertsScheduledForDeletion[]= clone $advert;
            $advert->setUser(null);
        }

        return $this;
    }

    /**
     * Clears out the collTransactionsRelatedByDebitUser collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addTransactionsRelatedByDebitUser()
     */
    public function clearTransactionsRelatedByDebitUser()
    {
        $this->collTransactionsRelatedByDebitUser = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collTransactionsRelatedByDebitUser collection loaded partially.
     */
    public function resetPartialTransactionsRelatedByDebitUser($v = true)
    {
        $this->collTransactionsRelatedByDebitUserPartial = $v;
    }

    /**
     * Initializes the collTransactionsRelatedByDebitUser collection.
     *
     * By default this just sets the collTransactionsRelatedByDebitUser collection to an empty array (like clearcollTransactionsRelatedByDebitUser());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initTransactionsRelatedByDebitUser($overrideExisting = true)
    {
        if (null !== $this->collTransactionsRelatedByDebitUser && !$overrideExisting) {
            return;
        }

        $collectionClassName = TransactionTableMap::getTableMap()->getCollectionClassName();

        $this->collTransactionsRelatedByDebitUser = new $collectionClassName;
        $this->collTransactionsRelatedByDebitUser->setModel('\MoonlightAdminEntity\MoonlightAdminEntity\Transaction');
    }

    /**
     * Gets an array of ChildTransaction objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildUser is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildTransaction[] List of ChildTransaction objects
     * @throws PropelException
     */
    public function getTransactionsRelatedByDebitUser(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collTransactionsRelatedByDebitUserPartial && !$this->isNew();
        if (null === $this->collTransactionsRelatedByDebitUser || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collTransactionsRelatedByDebitUser) {
                // return empty collection
                $this->initTransactionsRelatedByDebitUser();
            } else {
                $collTransactionsRelatedByDebitUser = ChildTransactionQuery::create(null, $criteria)
                    ->filterByUserRelatedByDebitUser($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collTransactionsRelatedByDebitUserPartial && count($collTransactionsRelatedByDebitUser)) {
                        $this->initTransactionsRelatedByDebitUser(false);

                        foreach ($collTransactionsRelatedByDebitUser as $obj) {
                            if (false == $this->collTransactionsRelatedByDebitUser->contains($obj)) {
                                $this->collTransactionsRelatedByDebitUser->append($obj);
                            }
                        }

                        $this->collTransactionsRelatedByDebitUserPartial = true;
                    }

                    return $collTransactionsRelatedByDebitUser;
                }

                if ($partial && $this->collTransactionsRelatedByDebitUser) {
                    foreach ($this->collTransactionsRelatedByDebitUser as $obj) {
                        if ($obj->isNew()) {
                            $collTransactionsRelatedByDebitUser[] = $obj;
                        }
                    }
                }

                $this->collTransactionsRelatedByDebitUser = $collTransactionsRelatedByDebitUser;
                $this->collTransactionsRelatedByDebitUserPartial = false;
            }
        }

        return $this->collTransactionsRelatedByDebitUser;
    }

    /**
     * Sets a collection of ChildTransaction objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $transactionsRelatedByDebitUser A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildUser The current object (for fluent API support)
     */
    public function setTransactionsRelatedByDebitUser(Collection $transactionsRelatedByDebitUser, ConnectionInterface $con = null)
    {
        /** @var ChildTransaction[] $transactionsRelatedByDebitUserToDelete */
        $transactionsRelatedByDebitUserToDelete = $this->getTransactionsRelatedByDebitUser(new Criteria(), $con)->diff($transactionsRelatedByDebitUser);


        $this->transactionsRelatedByDebitUserScheduledForDeletion = $transactionsRelatedByDebitUserToDelete;

        foreach ($transactionsRelatedByDebitUserToDelete as $transactionRelatedByDebitUserRemoved) {
            $transactionRelatedByDebitUserRemoved->setUserRelatedByDebitUser(null);
        }

        $this->collTransactionsRelatedByDebitUser = null;
        foreach ($transactionsRelatedByDebitUser as $transactionRelatedByDebitUser) {
            $this->addTransactionRelatedByDebitUser($transactionRelatedByDebitUser);
        }

        $this->collTransactionsRelatedByDebitUser = $transactionsRelatedByDebitUser;
        $this->collTransactionsRelatedByDebitUserPartial = false;

        return $this;
    }

    /**
     * Returns the number of related Transaction objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related Transaction objects.
     * @throws PropelException
     */
    public function countTransactionsRelatedByDebitUser(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collTransactionsRelatedByDebitUserPartial && !$this->isNew();
        if (null === $this->collTransactionsRelatedByDebitUser || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collTransactionsRelatedByDebitUser) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getTransactionsRelatedByDebitUser());
            }

            $query = ChildTransactionQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByUserRelatedByDebitUser($this)
                ->count($con);
        }

        return count($this->collTransactionsRelatedByDebitUser);
    }

    /**
     * Method called to associate a ChildTransaction object to this object
     * through the ChildTransaction foreign key attribute.
     *
     * @param  ChildTransaction $l ChildTransaction
     * @return $this|\MoonlightAdminEntity\MoonlightAdminEntity\User The current object (for fluent API support)
     */
    public function addTransactionRelatedByDebitUser(ChildTransaction $l)
    {
        if ($this->collTransactionsRelatedByDebitUser === null) {
            $this->initTransactionsRelatedByDebitUser();
            $this->collTransactionsRelatedByDebitUserPartial = true;
        }

        if (!$this->collTransactionsRelatedByDebitUser->contains($l)) {
            $this->doAddTransactionRelatedByDebitUser($l);

            if ($this->transactionsRelatedByDebitUserScheduledForDeletion and $this->transactionsRelatedByDebitUserScheduledForDeletion->contains($l)) {
                $this->transactionsRelatedByDebitUserScheduledForDeletion->remove($this->transactionsRelatedByDebitUserScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildTransaction $transactionRelatedByDebitUser The ChildTransaction object to add.
     */
    protected function doAddTransactionRelatedByDebitUser(ChildTransaction $transactionRelatedByDebitUser)
    {
        $this->collTransactionsRelatedByDebitUser[]= $transactionRelatedByDebitUser;
        $transactionRelatedByDebitUser->setUserRelatedByDebitUser($this);
    }

    /**
     * @param  ChildTransaction $transactionRelatedByDebitUser The ChildTransaction object to remove.
     * @return $this|ChildUser The current object (for fluent API support)
     */
    public function removeTransactionRelatedByDebitUser(ChildTransaction $transactionRelatedByDebitUser)
    {
        if ($this->getTransactionsRelatedByDebitUser()->contains($transactionRelatedByDebitUser)) {
            $pos = $this->collTransactionsRelatedByDebitUser->search($transactionRelatedByDebitUser);
            $this->collTransactionsRelatedByDebitUser->remove($pos);
            if (null === $this->transactionsRelatedByDebitUserScheduledForDeletion) {
                $this->transactionsRelatedByDebitUserScheduledForDeletion = clone $this->collTransactionsRelatedByDebitUser;
                $this->transactionsRelatedByDebitUserScheduledForDeletion->clear();
            }
            $this->transactionsRelatedByDebitUserScheduledForDeletion[]= clone $transactionRelatedByDebitUser;
            $transactionRelatedByDebitUser->setUserRelatedByDebitUser(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this User is new, it will return
     * an empty collection; or if this User has previously
     * been saved, it will retrieve related TransactionsRelatedByDebitUser from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in User.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildTransaction[] List of ChildTransaction objects
     */
    public function getTransactionsRelatedByDebitUserJoinAdvert(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildTransactionQuery::create(null, $criteria);
        $query->joinWith('Advert', $joinBehavior);

        return $this->getTransactionsRelatedByDebitUser($query, $con);
    }

    /**
     * Clears out the collTransactionsRelatedByCreditUser collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addTransactionsRelatedByCreditUser()
     */
    public function clearTransactionsRelatedByCreditUser()
    {
        $this->collTransactionsRelatedByCreditUser = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collTransactionsRelatedByCreditUser collection loaded partially.
     */
    public function resetPartialTransactionsRelatedByCreditUser($v = true)
    {
        $this->collTransactionsRelatedByCreditUserPartial = $v;
    }

    /**
     * Initializes the collTransactionsRelatedByCreditUser collection.
     *
     * By default this just sets the collTransactionsRelatedByCreditUser collection to an empty array (like clearcollTransactionsRelatedByCreditUser());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initTransactionsRelatedByCreditUser($overrideExisting = true)
    {
        if (null !== $this->collTransactionsRelatedByCreditUser && !$overrideExisting) {
            return;
        }

        $collectionClassName = TransactionTableMap::getTableMap()->getCollectionClassName();

        $this->collTransactionsRelatedByCreditUser = new $collectionClassName;
        $this->collTransactionsRelatedByCreditUser->setModel('\MoonlightAdminEntity\MoonlightAdminEntity\Transaction');
    }

    /**
     * Gets an array of ChildTransaction objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildUser is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildTransaction[] List of ChildTransaction objects
     * @throws PropelException
     */
    public function getTransactionsRelatedByCreditUser(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collTransactionsRelatedByCreditUserPartial && !$this->isNew();
        if (null === $this->collTransactionsRelatedByCreditUser || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collTransactionsRelatedByCreditUser) {
                // return empty collection
                $this->initTransactionsRelatedByCreditUser();
            } else {
                $collTransactionsRelatedByCreditUser = ChildTransactionQuery::create(null, $criteria)
                    ->filterByUserRelatedByCreditUser($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collTransactionsRelatedByCreditUserPartial && count($collTransactionsRelatedByCreditUser)) {
                        $this->initTransactionsRelatedByCreditUser(false);

                        foreach ($collTransactionsRelatedByCreditUser as $obj) {
                            if (false == $this->collTransactionsRelatedByCreditUser->contains($obj)) {
                                $this->collTransactionsRelatedByCreditUser->append($obj);
                            }
                        }

                        $this->collTransactionsRelatedByCreditUserPartial = true;
                    }

                    return $collTransactionsRelatedByCreditUser;
                }

                if ($partial && $this->collTransactionsRelatedByCreditUser) {
                    foreach ($this->collTransactionsRelatedByCreditUser as $obj) {
                        if ($obj->isNew()) {
                            $collTransactionsRelatedByCreditUser[] = $obj;
                        }
                    }
                }

                $this->collTransactionsRelatedByCreditUser = $collTransactionsRelatedByCreditUser;
                $this->collTransactionsRelatedByCreditUserPartial = false;
            }
        }

        return $this->collTransactionsRelatedByCreditUser;
    }

    /**
     * Sets a collection of ChildTransaction objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $transactionsRelatedByCreditUser A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildUser The current object (for fluent API support)
     */
    public function setTransactionsRelatedByCreditUser(Collection $transactionsRelatedByCreditUser, ConnectionInterface $con = null)
    {
        /** @var ChildTransaction[] $transactionsRelatedByCreditUserToDelete */
        $transactionsRelatedByCreditUserToDelete = $this->getTransactionsRelatedByCreditUser(new Criteria(), $con)->diff($transactionsRelatedByCreditUser);


        $this->transactionsRelatedByCreditUserScheduledForDeletion = $transactionsRelatedByCreditUserToDelete;

        foreach ($transactionsRelatedByCreditUserToDelete as $transactionRelatedByCreditUserRemoved) {
            $transactionRelatedByCreditUserRemoved->setUserRelatedByCreditUser(null);
        }

        $this->collTransactionsRelatedByCreditUser = null;
        foreach ($transactionsRelatedByCreditUser as $transactionRelatedByCreditUser) {
            $this->addTransactionRelatedByCreditUser($transactionRelatedByCreditUser);
        }

        $this->collTransactionsRelatedByCreditUser = $transactionsRelatedByCreditUser;
        $this->collTransactionsRelatedByCreditUserPartial = false;

        return $this;
    }

    /**
     * Returns the number of related Transaction objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related Transaction objects.
     * @throws PropelException
     */
    public function countTransactionsRelatedByCreditUser(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collTransactionsRelatedByCreditUserPartial && !$this->isNew();
        if (null === $this->collTransactionsRelatedByCreditUser || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collTransactionsRelatedByCreditUser) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getTransactionsRelatedByCreditUser());
            }

            $query = ChildTransactionQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByUserRelatedByCreditUser($this)
                ->count($con);
        }

        return count($this->collTransactionsRelatedByCreditUser);
    }

    /**
     * Method called to associate a ChildTransaction object to this object
     * through the ChildTransaction foreign key attribute.
     *
     * @param  ChildTransaction $l ChildTransaction
     * @return $this|\MoonlightAdminEntity\MoonlightAdminEntity\User The current object (for fluent API support)
     */
    public function addTransactionRelatedByCreditUser(ChildTransaction $l)
    {
        if ($this->collTransactionsRelatedByCreditUser === null) {
            $this->initTransactionsRelatedByCreditUser();
            $this->collTransactionsRelatedByCreditUserPartial = true;
        }

        if (!$this->collTransactionsRelatedByCreditUser->contains($l)) {
            $this->doAddTransactionRelatedByCreditUser($l);

            if ($this->transactionsRelatedByCreditUserScheduledForDeletion and $this->transactionsRelatedByCreditUserScheduledForDeletion->contains($l)) {
                $this->transactionsRelatedByCreditUserScheduledForDeletion->remove($this->transactionsRelatedByCreditUserScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildTransaction $transactionRelatedByCreditUser The ChildTransaction object to add.
     */
    protected function doAddTransactionRelatedByCreditUser(ChildTransaction $transactionRelatedByCreditUser)
    {
        $this->collTransactionsRelatedByCreditUser[]= $transactionRelatedByCreditUser;
        $transactionRelatedByCreditUser->setUserRelatedByCreditUser($this);
    }

    /**
     * @param  ChildTransaction $transactionRelatedByCreditUser The ChildTransaction object to remove.
     * @return $this|ChildUser The current object (for fluent API support)
     */
    public function removeTransactionRelatedByCreditUser(ChildTransaction $transactionRelatedByCreditUser)
    {
        if ($this->getTransactionsRelatedByCreditUser()->contains($transactionRelatedByCreditUser)) {
            $pos = $this->collTransactionsRelatedByCreditUser->search($transactionRelatedByCreditUser);
            $this->collTransactionsRelatedByCreditUser->remove($pos);
            if (null === $this->transactionsRelatedByCreditUserScheduledForDeletion) {
                $this->transactionsRelatedByCreditUserScheduledForDeletion = clone $this->collTransactionsRelatedByCreditUser;
                $this->transactionsRelatedByCreditUserScheduledForDeletion->clear();
            }
            $this->transactionsRelatedByCreditUserScheduledForDeletion[]= clone $transactionRelatedByCreditUser;
            $transactionRelatedByCreditUser->setUserRelatedByCreditUser(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this User is new, it will return
     * an empty collection; or if this User has previously
     * been saved, it will retrieve related TransactionsRelatedByCreditUser from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in User.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildTransaction[] List of ChildTransaction objects
     */
    public function getTransactionsRelatedByCreditUserJoinAdvert(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildTransactionQuery::create(null, $criteria);
        $query->joinWith('Advert', $joinBehavior);

        return $this->getTransactionsRelatedByCreditUser($query, $con);
    }

    /**
     * Clears the current object, sets all attributes to their default values and removes
     * outgoing references as well as back-references (from other objects to this one. Results probably in a database
     * change of those foreign objects when you call `save` there).
     */
    public function clear()
    {
        $this->id = null;
        $this->username = null;
        $this->password = null;
        $this->firstname = null;
        $this->lastname = null;
        $this->address = null;
        $this->verified = null;
        $this->datecreated = null;
        $this->lastmodified = null;
        $this->alreadyInSave = false;
        $this->clearAllReferences();
        $this->applyDefaultValues();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);
    }

    /**
     * Resets all references and back-references to other model objects or collections of model objects.
     *
     * This method is used to reset all php object references (not the actual reference in the database).
     * Necessary for object serialisation.
     *
     * @param      boolean $deep Whether to also clear the references on all referrer objects.
     */
    public function clearAllReferences($deep = false)
    {
        if ($deep) {
            if ($this->collAdverts) {
                foreach ($this->collAdverts as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collTransactionsRelatedByDebitUser) {
                foreach ($this->collTransactionsRelatedByDebitUser as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collTransactionsRelatedByCreditUser) {
                foreach ($this->collTransactionsRelatedByCreditUser as $o) {
                    $o->clearAllReferences($deep);
                }
            }
        } // if ($deep)

        $this->collAdverts = null;
        $this->collTransactionsRelatedByDebitUser = null;
        $this->collTransactionsRelatedByCreditUser = null;
    }

    /**
     * Return the string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->exportTo(UserTableMap::DEFAULT_STRING_FORMAT);
    }

    /**
     * Code to be run before persisting the object
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preSave(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preSave')) {
            return parent::preSave($con);
        }
        return true;
    }

    /**
     * Code to be run after persisting the object
     * @param ConnectionInterface $con
     */
    public function postSave(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postSave')) {
            parent::postSave($con);
        }
    }

    /**
     * Code to be run before inserting to database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preInsert(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preInsert')) {
            return parent::preInsert($con);
        }
        return true;
    }

    /**
     * Code to be run after inserting to database
     * @param ConnectionInterface $con
     */
    public function postInsert(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postInsert')) {
            parent::postInsert($con);
        }
    }

    /**
     * Code to be run before updating the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preUpdate(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preUpdate')) {
            return parent::preUpdate($con);
        }
        return true;
    }

    /**
     * Code to be run after updating the object in database
     * @param ConnectionInterface $con
     */
    public function postUpdate(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postUpdate')) {
            parent::postUpdate($con);
        }
    }

    /**
     * Code to be run before deleting the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preDelete(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preDelete')) {
            return parent::preDelete($con);
        }
        return true;
    }

    /**
     * Code to be run after deleting the object in database
     * @param ConnectionInterface $con
     */
    public function postDelete(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postDelete')) {
            parent::postDelete($con);
        }
    }


    /**
     * Derived method to catches calls to undefined methods.
     *
     * Provides magic import/export method support (fromXML()/toXML(), fromYAML()/toYAML(), etc.).
     * Allows to define default __call() behavior if you overwrite __call()
     *
     * @param string $name
     * @param mixed  $params
     *
     * @return array|string
     */
    public function __call($name, $params)
    {
        if (0 === strpos($name, 'get')) {
            $virtualColumn = substr($name, 3);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }

            $virtualColumn = lcfirst($virtualColumn);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }
        }

        if (0 === strpos($name, 'from')) {
            $format = substr($name, 4);

            return $this->importFrom($format, reset($params));
        }

        if (0 === strpos($name, 'to')) {
            $format = substr($name, 2);
            $includeLazyLoadColumns = isset($params[0]) ? $params[0] : true;

            return $this->exportTo($format, $includeLazyLoadColumns);
        }

        throw new BadMethodCallException(sprintf('Call to undefined method: %s.', $name));
    }

}
