<?php

namespace MoonlightAdminEntity\MoonlightAdminEntity;

use MoonlightAdminEntity\MoonlightAdminEntity\Base\AdvertQuery as BaseAdvertQuery;

/**
 * Skeleton subclass for performing query and update operations on the 'advert' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class AdvertQuery extends BaseAdvertQuery
{

}
