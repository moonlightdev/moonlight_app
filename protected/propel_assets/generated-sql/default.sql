
# This is a fix for InnoDB in MySQL >= 4.1.x
# It "suspends judgement" for fkey relationships until are tables are set.
SET FOREIGN_KEY_CHECKS = 0;

-- ---------------------------------------------------------------------
-- advert
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `advert`;

CREATE TABLE `advert`
(
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `type` VARCHAR(5) NOT NULL,
    `rate` VARCHAR(5) NOT NULL,
    `api` VARCHAR(10) NOT NULL,
    `datecreated` DATETIME NOT NULL,
    `lastmodified` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `createdby` INTEGER NOT NULL,
    PRIMARY KEY (`id`),
    INDEX `createdby` (`createdby`),
    CONSTRAINT `advert_ibfk_1`
        FOREIGN KEY (`createdby`)
        REFERENCES `user` (`id`)
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- appconfig
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `appconfig`;

CREATE TABLE `appconfig`
(
    `version` INTEGER NOT NULL
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- exchange
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `exchange`;

CREATE TABLE `exchange`
(
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `name` VARCHAR(50) NOT NULL,
    `datecreated` DATETIME NOT NULL,
    `lastmodified` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY (`id`)
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- transaction
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `transaction`;

CREATE TABLE `transaction`
(
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `debit_user` INTEGER NOT NULL,
    `credit_user` INTEGER NOT NULL,
    `amount` VARCHAR(50) NOT NULL,
    `description` TEXT NOT NULL,
    `datecreated` DATETIME NOT NULL,
    `lastmodified` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `advert_id` INTEGER NOT NULL,
    PRIMARY KEY (`id`),
    INDEX `debit_user` (`debit_user`),
    INDEX `credit_user` (`credit_user`),
    INDEX `advert` (`advert_id`),
    CONSTRAINT `transaction_ibfk_1`
        FOREIGN KEY (`debit_user`)
        REFERENCES `user` (`id`),
    CONSTRAINT `transaction_ibfk_2`
        FOREIGN KEY (`credit_user`)
        REFERENCES `user` (`id`),
    CONSTRAINT `transaction_ibfk_3`
        FOREIGN KEY (`advert_id`)
        REFERENCES `advert` (`id`)
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- user
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `user`;

CREATE TABLE `user`
(
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `username` VARCHAR(50) NOT NULL,
    `password` VARCHAR(200) NOT NULL,
    `firstname` VARCHAR(50) NOT NULL,
    `lastname` VARCHAR(50) NOT NULL,
    `address` TEXT NOT NULL,
    `verified` TINYINT(1) NOT NULL,
    `datecreated` DATETIME NOT NULL,
    `lastmodified` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY (`id`)
) ENGINE=InnoDB;

# This restores the fkey checks, after having unset them earlier
SET FOREIGN_KEY_CHECKS = 1;
